<?php
//Source: http://www.tiposaurus.co.uk/php/2012/04/16/generate-a-month-calendar-in-php.html
function showMonth($month, $year, $database)
{
  $sumaMonth=0;
  $date = mktime(12, 0, 0, $month, 1, $year);
  $daysInMonth = date("t", $date);
  // calculate the position of the first day in the calendar (sunday = 1st column, etc)
  $offset = date("w", $date);
  $rows = 1;
   
  //echo "<h1>Displaying calendar for " . date("F Y", $date) . "</h1>\n";
   
  echo "<table id=\"calendar\" border=\"1\">\n";
  echo "\t<tr><th>Do</th><th>Lu</th><th>Ma</th><th>Mi</th><th>Ju</th><th>Vi</th><th>Sa</th></tr>";
  echo "\n\t<tr>";
   
  for($i = 1; $i <= $offset; $i++)
  {
    echo "<td></td>";
  }
  
  for($day = 1; $day <= $daysInMonth; $day++)
  {
    if( ($day + $offset - 1) % 7 == 0 && $day != 1)
    {
      echo "</tr>\n\t<tr>";
      $rows++;
    }

    echo "<td>";
      echo "<div>".$day."</div>";

      $database->query("SELECT * FROM history WHERE year='$year' AND month='$month' AND day='$day'");
      $sumaDay=0;
      foreach ($database->getAll() as $row) {
        $sumaDay = $sumaDay+$row['hours'];
        $sumaMonth = $sumaMonth + $row['hours'];
      }
      if( $sumaDay>=1 ){
        echo "<span style=\"font-size:10px; width: 50px; display: inline-block;\">".$sumaDay." horas</span>";
      }else{
        echo "<span style=\"font-size:10px; width: 50px; display: inline-block;\"></span>";
      }
    echo "</td>";
  }

  while( ($day + $offset) <= $rows * 7)
  {
    echo "<td></td>";
    $day++;
  }

  echo "</tr>\n";
  echo "</table>\n";

  echo '<h3 class="mt-2">'.$sumaMonth.' de 160 <small>horas trabajadas</small></h3>';
  //echo '<p class="mb-4"><small>20 horas de comida no se cuentan</small></p>';
}
?>